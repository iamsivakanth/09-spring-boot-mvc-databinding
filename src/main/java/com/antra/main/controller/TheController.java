package com.antra.main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.antra.main.model.UserLoginBean;

@Controller
public class TheController {

	@GetMapping(value = "/login")
	public String getLogin(Model model) {
		UserLoginBean user = new UserLoginBean();
		model.addAttribute("user", user);
		return "Login";
	}

	@PostMapping(value = "/verify")
	public String verify(@ModelAttribute(value = "user") UserLoginBean userBean, Model model) {
		String username = userBean.getUsername();
		String password = userBean.getPassword();

		if (username.equals("Shiv123") && password.equals("1234@"))
			return "Success";
		else {
			model.addAttribute("message", "Invalid Username or Password");
			return "Login";
		}
	}
}