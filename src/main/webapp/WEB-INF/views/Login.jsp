<%@ page language="java" isELIgnored="false"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<c:if test="${message ne null}">
		<h2 style="color: red;">
			<c:out value="${message}" />
		</h2>
	</c:if>
	<form:form action="/verify" method="post" modelAttribute="user">
		Username : <form:input path="username" />
		<br>
		Password : <form:password path="password" />
		<br>
		<input type="submit" value="Submit">
	</form:form>
</body>
</html>